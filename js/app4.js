var btnbuscar = document.querySelector('#btnbuscar');
var btnlimpiar = document.querySelector('#btnlimpiar');
var show = document.createElement('tr');
btnbuscar.addEventListener('click',()=>{
    var information = parseInt(document.querySelector('.information').value);
    
    if(isNaN(information)){
        alert('INGRESA UN NUMERO')
    }else{
    var http = new XMLHttpRequest();
    http.open('GET', 'https://jsonplaceholder.typicode.com/users/'+information, true);
   
    http.onreadystatechange = function() {
    
        if (http.readyState == 4 && http.status == 200) {
      
            var datos = JSON.parse(http.responseText);
            console.log(datos);
            show.classList.add('table-row');
            show.innerHTML ='<tr><th>'+datos.id+'</th>'+'<th>'
            +datos.name+'</th>'+'<th>'+datos.username+'</th>'+'<th>'+
            datos.email+'</th>'+'<th>'+datos.phone+'</th>'+'<th>'+datos.website+'</th>'+'<th>'+datos.company.bs+','+
            datos.company.cathPhrase+','+datos.company.name+'</th>'+'<th>'+datos.address.city+','+datos.address.geo.lat+','+datos.address.geo.lng+','+datos.address.street+','+datos.address.suite+','+datos.address.zipcode+'</th>'+'</tr>'
            document.querySelector('#tabla').append(show);
        } else if (http.status == 404) {
            alert('EL NUMERO NO EXISTE');
        }
}

http.send();
}
});

btnlimpiar.addEventListener('click', ()=>{
    show.innerHTML = "";
    document.querySelector('.information').value="";
})
